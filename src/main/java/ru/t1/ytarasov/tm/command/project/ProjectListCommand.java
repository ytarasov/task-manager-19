package ru.t1.ytarasov.tm.command.project;

import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Show project list";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        List<Project> projects = getProjectService().findAll(sort);
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
