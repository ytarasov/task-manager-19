package ru.t1.ytarasov.tm.command.task;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String NAME ="task-create";

    public static final String DESCRIPTION = "Create task.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
