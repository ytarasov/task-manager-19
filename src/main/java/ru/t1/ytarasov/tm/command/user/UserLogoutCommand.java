package ru.t1.ytarasov.tm.command.user;

import ru.t1.ytarasov.tm.exception.AbstractException;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "User logout ";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
