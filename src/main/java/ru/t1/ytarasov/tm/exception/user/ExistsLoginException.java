package ru.t1.ytarasov.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! This login already exists...");
    }
}
