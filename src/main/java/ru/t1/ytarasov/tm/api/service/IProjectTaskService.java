package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(String taskId, String projectId) throws AbstractException;

    void removeProjectById(String projectId) throws AbstractException;

    void removeProjectByIndex(Integer projectIndex) throws AbstractException;

    void clearAllProjects() throws AbstractException;

    void unbindTaskFromProject(String taskId, String projectId) throws AbstractException;

}
