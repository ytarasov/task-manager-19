package ru.t1.ytarasov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);
}
