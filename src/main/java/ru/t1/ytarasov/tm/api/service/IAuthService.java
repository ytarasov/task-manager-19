package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email) throws AbstractException;

    void login(String login, String password) throws AbstractException;

    void logout();

    Boolean isAuth();

    String getUserId() throws AbstractException;

    User getUser() throws AbstractException;

}
