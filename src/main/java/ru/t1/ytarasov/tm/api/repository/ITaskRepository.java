package ru.t1.ytarasov.tm.api.repository;

import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, Status status);

    Task create(String name, String description, Status status);

    List<Task> findAllTasksByProjectId(String projectId);

}
