package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command) throws AbstractException;

    AbstractCommand getCommandByName(String name) throws AbstractException;

    AbstractCommand getCommandByArgument(String argument) throws AbstractException;

    Collection<AbstractCommand> getTerminalCommands();

}
