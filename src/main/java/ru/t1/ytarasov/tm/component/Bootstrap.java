package ru.t1.ytarasov.tm.component;

import ru.t1.ytarasov.tm.api.repository.ICommandRepository;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.command.project.*;
import ru.t1.ytarasov.tm.command.system.*;
import ru.t1.ytarasov.tm.command.task.*;
import ru.t1.ytarasov.tm.command.user.*;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ytarasov.tm.exception.system.CommandNotSupportedException;
import ru.t1.ytarasov.tm.repository.CommandRepository;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;
import ru.t1.ytarasov.tm.repository.UserRepository;
import ru.t1.ytarasov.tm.service.*;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        try {
            registry(new ApplicationAboutCommand());
            registry(new ApplicationHelpCommand());
            registry(new ApplicationVersionCommand());
            registry(new CommandListCommand());
            registry(new ArgumentListCommand());
            registry(new ApplicationInfoCommand());
            registry(new ApplicationExitCommand());

            registry(new ProjectCreateCommand());
            registry(new ProjectListCommand());
            registry(new ProjectShowByIdCommand());
            registry(new ProjectShowByIndexCommand());
            registry(new ProjectUpdateByIdCommand());
            registry(new ProjectUpdateByIndexCommand());
            registry(new ProjectRemoveByIdCommand());
            registry(new ProjectRemoveByIndexCommand());
            registry(new ProjectClearCommand());
            registry(new ProjectStartByIdCommand());
            registry(new ProjectStartByIndexCommand());
            registry(new ProjectCompleteByIdCommand());
            registry(new ProjectCompleteByIndexCommand());
            registry(new ProjectChangeStatusById());
            registry(new ProjectChangeStatusByIndex());

            registry(new TaskCreateCommand());
            registry(new TaskListCommand());
            registry(new TaskClearCommand());
            registry(new TaskShowByIdCommand());
            registry(new TaskShowByIndexCommand());
            registry(new TaskUpdateByIdCommand());
            registry(new TaskUpdateByIndexCommand());
            registry(new TaskRemoveByIdCommand());
            registry(new TaskRemoveByIndexCommand());
            registry(new TaskStartByIdCommand());
            registry(new TaskStartByIndexCommand());
            registry(new TaskCompleteByIdCommand());
            registry(new TaskCompleteByIndexCommand());
            registry(new TaskChangeStatusByIdCommand());
            registry(new TaskChangeStatusByIndexCommand());
            registry(new TaskBindToProjectCommand());
            registry(new TaskUnbindFromProjectCommand());

            registry(new UserRegistryCommand());
            registry(new UserChangePasswordCommand());
            registry(new UserLoginCommand());
            registry(new UserLogoutCommand());
            registry(new UserUpdateProfileCommand());
            registry(new UserViewProfileCommand());
        }
        catch (AbstractException e) {
            getLoggerService().error(e);
            System.out.println();
        }
    }

    public void runArguments(String[] args) {
        if (args == null || args.length == 0) {
            runCommands();
            return;
        }
        final String arg = args[0];
        try {
            runArgument(arg);
        } catch (final AbstractException e) {
            getLoggerService().error(e);
            System.out.println();
        }
    }

    private void runArgument(final String arg) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void runCommands() {
        initLogger();
        System.out.println();
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                String command = TerminalUtil.nextLine();
                runCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                getLoggerService().error(e);
                System.out.println();
                System.out.println("[FAIL]");
            }
        }
    }

    private void runCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    private void initDemoData() {
        try {
            userService.create("TEST", "TEST", "test@mail.ru");
            userService.create("TEST2", "TEST2", "test2@mail.ru");
            userService.create("ADMIN", "ADMIN", Role.ADMIN);

            projectService.create("BETA PROJECT", "This is beta project", Status.IN_PROGRESS);
            projectService.create("ALPHABET", "This is alphabet project", Status.COMPLETED);
            projectService.create("ANT", "This is ant project", Status.NOT_STARTED);
            taskService.create("MAIN TASK", "This is main task", Status.IN_PROGRESS);
            taskService.create("ZED TASK", "This is zed task", Status.NOT_STARTED);
            taskService.create("SECONDARY TASK", "This is secondary task", Status.COMPLETED);
        } catch (AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return this.loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void registry(final AbstractCommand command) throws AbstractException {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
