package ru.t1.ytarasov.tm.service;

import ru.t1.ytarasov.tm.api.repository.IRepository;
import ru.t1.ytarasov.tm.api.service.IService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.IndexIncorrectException;
import ru.t1.ytarasov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) findAll();
        return repository.findAll(comparator);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    public M add(final M model) {
        if (model == null) return null;
        return add(model);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(Integer index) throws AbstractException {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public M removeById(String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(Integer index) throws AbstractException {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public List<M> findAll(Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

}
