package ru.t1.ytarasov.tm.service;

import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.add(new Task(name, description));
    }

    @Override
    public Task create(final String name, Status status) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.add(new Task(name, status));
    }

    @Override
    public Task create(final String name, final String description, final Status status) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.add(new Task(name, description, status));
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllTasksByProjectId(projectId);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}
